#!/bin/bash
###################################################
#Install phantomsim
###################################################
if [ ! -e conf.sh ];then
    . conf.sh.temp
    echo "Creating configuration file..."
    cp conf.sh.temp conf.sh
else
    . conf.sh
fi

echo "Changing permissions for phantomsim scripts..."
chmod +x bin/ph*

echo "Done."
