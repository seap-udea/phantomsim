BRANCH=$(shell git branch | grep "*" | cut -f 2 -d '*')

clean:
	find . -name "*~" -exec rm -r {} \;

commit:
	@echo "Commiting..."
	@-git commit -am "Commit"
	@-git push origin $(BRANCH)

pull:
	@echo "Pulling latest version..."
	@-git reset --hard HEAD
	@-git pull origin $(BRANCH)

update:clean
	@echo "Updating..."
	@-git add bin/*
	@-phpatch pack
	@-make commit

unpack:
	@echo "Unpacking patch..."
	@-phpatch unpack
