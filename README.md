# Managing Phantom Simulations

This package contains a set of useful scripts to manage your phantom
simulations in local and remote machines.

For a more complete guide to use `phantomsim` please refere to the 
[Phantomsim Wiki](https://bitbucket.org/seap-udea/phantomsim/wiki/Home).

---

## Obtain a copy

To obtain a copy of ``phantomsim`` you may:

1. Get the latest release of the package from [this
   link](https://bitbucket.org/seap-udea/phantomsim/downloads)

2. Clone anonymously the repository:

   	 `git clone https://seap-udea@bitbucket.org/seap-udea/phantomsim.git`

3. Clone the repository if you are a contributor:

   	 `git clone git@bitbucket.org:seap-udea/phantomsim.git`

See [Clone a repository](https://confluence.atlassian.com/x/4whODQ) if
you need some help.

